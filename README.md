# EXAMPLE 001

This is a demonstration of how links in an electronic science notebook might reference code in a Git repository.

Later, I edited the code and commit a new revision. 

It may be simpler in the notebook to record the repository at the top and then just note the commit in the text. The user will then need to go to the repository and find that commit themselves. To be honest, anyone using Git/GitLab in any serious fashion will be able to do that.

Version 3